extends CanvasLayer


# Called when the node enters the scene tree for the first time.
func _ready():
	$Dialog.visible = false

func set_dialog_text(name, text):
	$Dialog.visible = true
	$Dialog.set_text(name + ": " + text)

func end_dialog():
	$Dialog.visible = false
