extends Area2D
signal player_action

export var speed = 200
export var tile_size = 64

enum State {
	IDLE_UP,
	IDLE_RIGHT,
	IDLE_DOWN,
	IDLE_LEFT,
	WALK_UP,
	WALK_RIGHT,
	WALK_DOWN,
	WALK_LEFT
}

var move_to = null
var delta
var state



# Called when the node enters the scene tree for the first time.
func _ready():
	_set_idle_state()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if GameState.current_state != GameState.State.MOVING:
		return
	delta = _delta
	_check_input()
	_animate()
	_move()

func _check_input():
	if _is_moving():
		return

	_check_action_input()
	_check_movement_input()

func _check_action_input():
	var pressed_action = Input.is_action_just_pressed("ui_select") 
	var is_colliding = $RayCast2D.is_colliding()

	if pressed_action && is_colliding:
		emit_signal("player_action", $RayCast2D.get_collider())

func _check_movement_input():
	if Input.is_action_pressed("ui_down"):
		$RayCast2D.rotation_degrees = 90
		state = State.WALK_DOWN if _can_walk() else State.IDLE_DOWN
	elif Input.is_action_pressed("ui_up"):
		$RayCast2D.rotation_degrees = -90
		state = State.WALK_UP if _can_walk() else State.IDLE_UP
	elif Input.is_action_pressed("ui_right"):
		$RayCast2D.rotation_degrees = 0
		state = State.WALK_RIGHT if _can_walk() else State.IDLE_RIGHT
	elif Input.is_action_pressed("ui_left"):
		$RayCast2D.rotation_degrees = 180
		state = State.WALK_LEFT if _can_walk() else State.IDLE_LEFT

func _can_walk():
	$RayCast2D.force_raycast_update()

	return not $RayCast2D.is_colliding()

func _is_moving():
	var walking_states = [
		State.WALK_UP,
		State.WALK_RIGHT,
		State.WALK_DOWN,
		State.WALK_LEFT
	]

	return walking_states.has(state)

func _move():
	if not _is_moving():
		return
		
	if move_to == null:
		move_to = position + _get_direction() * tile_size

	position = position.move_toward(move_to, delta * speed)

	if position == move_to:
		move_to = null
		_set_idle_state()

func _get_direction():
	match state:
		State.IDLE_UP, State.WALK_UP:
			return Vector2.UP
		State.IDLE_RIGHT, State.WALK_RIGHT:
			return Vector2.RIGHT
		State.IDLE_DOWN, State.WALK_DOWN:
			return Vector2.DOWN
		State.IDLE_LEFT, State.WALK_LEFT:
			return Vector2.LEFT

func _set_idle_state():
	match state:
		State.WALK_UP:
			state = State.IDLE_UP
		State.WALK_RIGHT:
			state = State.IDLE_RIGHT
		State.WALK_DOWN:
			state = State.IDLE_DOWN
		State.WALK_LEFT:
			state = State.IDLE_LEFT
		_:
			state = State.IDLE_RIGHT

func _animate():
	match state:
		State.WALK_UP:
			$AnimatedSprite.animation = "up-walk"
		State.WALK_RIGHT:
			$AnimatedSprite.animation = "right-walk"
			$AnimatedSprite.flip_h = false
		State.WALK_DOWN:
			$AnimatedSprite.animation = "down-walk"
		State.WALK_LEFT:
			$AnimatedSprite.animation = "right-walk"
			$AnimatedSprite.flip_h = true
		State.IDLE_UP:
			$AnimatedSprite.animation = "up-idle"
		State.IDLE_RIGHT:
			$AnimatedSprite.animation = "right-idle"
			$AnimatedSprite.flip_h = false
		State.IDLE_DOWN:
			$AnimatedSprite.animation = "down-idle"
		State.IDLE_LEFT:
			$AnimatedSprite.animation = "right-idle"
			$AnimatedSprite.flip_h = true
