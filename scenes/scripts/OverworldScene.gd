extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	GameState.current_state = GameState.State.MOVING

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if GameState.current_state == GameState.State.ENDING_DIALOG:
		GameState.current_state = GameState.State.MOVING
	if GameState.current_state == GameState.State.DIALOG:
		_check_dialog_input()

func _check_dialog_input():
	if Input.is_action_just_pressed("ui_select"):
		$OverworldHUD.end_dialog()
		GameState.current_state = GameState.State.ENDING_DIALOG

func _on_OverworldPlayer_player_action(collider):
	if collider.has_method("get_dialog"):
		var name = collider.character_name
		var dialog = collider.get_dialog()
		$OverworldHUD.set_dialog_text(name, dialog)
		GameState.current_state = GameState.State.DIALOG
